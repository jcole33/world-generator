import { Ref, SetupContext } from 'vue'
import { Attribute, RunningAttribute } from 'src/types/models'
import { QVueGlobals, useQuasar } from 'quasar'
import CustomDialog from './CustomDialog.vue'


export function arrayEvents<I, T>(list: Ref<I[]>, eventName: T, context: SetupContext<T[]>) {
  const $q = useQuasar()
  function remove(index: number) {
    if (list.value.length < 2) return
    $q.dialog({
      component: CustomDialog,
      componentProps: {
        title: 'Confirm Delete',
        message: 'Are you sure you want to delete this?',
        okProps: {
          label: 'Delete',
          color: 'negative'
        },
        cancel: true,
        cancelProps: {
          label: 'Cancel',
          color: 'primary'
        }
      }
    }).onOk(() => {
      const newArray = [...list.value]
      newArray.splice(index, 1)
      context.emit(eventName, newArray)
    })

  }
  function update(index: number, updated: I) {
    const newArray = [...list.value]
    newArray.splice(index, 1, updated)
    context.emit(eventName, newArray)
  }
  function insert(added: I) {
    context.emit(eventName, [...list.value, added])
  }
  return { remove, update, insert }
}

export function objectEvents<I, K extends keyof I, T>(object: Ref<I>, eventName: T, context: SetupContext<T[]>) {
  function update(index: K, updated: I[K]) {
    context.emit(eventName, { ...object.value, [index]: updated })
  }
  return { update }
}


export function generationMethods() {
  function generateValue(attribute: Attribute) {
    let pickIndex: number
    if (attribute.uniform) {
      pickIndex = Math.floor(Math.random() * attribute.options.length)
    } else {
      const random = Math.floor(Math.random() * 100 + 1)
      const rangeSize = 100 / attribute.options.length
      pickIndex = Math.floor(random / rangeSize)
    }
    return attribute.options[pickIndex].name

  }
  function generateValues(attributes: RunningAttribute[]) {
    attributes.forEach((attribute) => {
      if (!attribute.locked)
        attribute.value = generateValue(attribute)
    })
  }
  return { generateValue, generateValues }
}



export function displayErrorMessage(errorMessage: string, $q: QVueGlobals) {
  $q.dialog({
    component: CustomDialog,
    componentProps: {
      title: 'Error!',
      message: errorMessage
    }
  })
  return false
}