export interface Option {
  name: string
  probability?: number
}

export interface Attribute {
  name: string
  uniform: boolean
  options: Option[]
}


export interface Generator {
  id: Readonly<string>
  type: string
  name: string
  attributes: Attribute[]
}

export type RunningOption = Required<Option>

export interface RunningAttribute extends Attribute {
  options: RunningOption[]
  included: boolean
  value: string
  locked: boolean
}

export interface RunningGenerator extends Generator {
  attributes: RunningAttribute[]
}

type RecursiveRequired<T> = {
  [P in keyof T]-?: T[P] extends (infer U)[]
  ? RecursiveRequired<U>[]
  : T[P] extends object
  ? // eslint-disable-next-line @typescript-eslint/no-unused-vars
  RecursiveRequired<T[P]>
  : T[P]
}
