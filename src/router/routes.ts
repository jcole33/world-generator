import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
		path: '/',
		redirect: '/home'
	},
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'home', name: 'Home', component: () => import('pages/Index.vue') },
      { path: 'generator/:id([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})', name: 'EditGenerator', component: () => import('pages/GeneratorPage.vue') }
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
